import { Component } from "react";
import validator from "validator";

import Success from "../Success"



import "./index.css"

class Form extends Component {
    state = {
        studentName: "",
        collegeName: "",
        branch: "",
        gender: "",
        dateOfBirth: "",
        address: "",
        emailErr: "" ,
        phoneNumErr:"",
        Error:{},
        isValidEmail: false,
        isValidNumber:false,
        isSubmitted: false

    }


    onstudentName = (event) => {
        this.setState({ studentName: event.target.value })

    }

    onCollegeName = (event) => {
        this.setState({ collegeName: event.target.value })
    }

    onbrachInput = (event) => {
        this.setState({ branch: event.target.value })
    }

    onMaleGenderInput = (event) => {
        this.setState({ gender: event.target.value })
    }

    onFemaleGenderInput = (event) => {
        this.setState({ gender: event.target.value })
    }

    onDateOfBirth = (event) => {
        this.setState({ dateOfBirth: event.target.value })
    }

    onEmailInput = (event) => {
        if (validator.isEmail(event.target.value) === true) {
            this.setState({ emailId: event.target.value,isValidEmail:true })
            this.setState((prevState) =>{
                {prevState.Error.emailErr = ""}
            })

        } else {
            this.setState({emailId: event.target.value,isValidEmail:false})
            this.setState((prevState) =>{
                {prevState.Error.emailErr = "please enter valid mail"}
            })
        }
    }

    onPhoneNumberInput = (event) => {
        if(event.target.value.length === 10){
            this.setState({ phone: event.target.value ,isValidNumber:true})
            this.setState((prevState) =>{
                {prevState.Error.phoneNumErr = ""}
            })
        }else{
            this.setState({ phone: event.target.value,isValidNumber:false})
            this.setState((prevState) =>{
                {prevState.Error.phoneNumErr = "please enter valid number"}
            })
        }
        
    }
    onAddressInput = (event) => {
        this.setState({ address: event.target.value })
    }

    onAddStudent = (event) => {
        event.preventDefault()
        const {studentName, branch,collegeName,isValidEmail, gender,emailErr,phoneNumErr,address,dateOfBirth,isValidNumber,Error} = this.state
        
        if(studentName ===""){
            Error.nameError = "Required*"
        }
        
        if(collegeName===""){
            Error.clgNameErr = "Required*"
        }
        
        if(branch ===""){
            Error.branchErr = "Required*"
        }

        if(gender === ""){
            Error.genderErr = "Required*"
        }

        if(dateOfBirth ===""){
            Error.DOBErr = "Required*"
        }
        if(emailErr === ""){
            Error.emailErr = "Required"
        }
        if(phoneNumErr === ""){
            Error.phoneNumErr = "Required*"
        }
        
        if(address === ""){
            Error.addressErr = "Required*"
        }
        if(isValidEmail && isValidNumber){
             this.setState({isSubmitted:true})
        }

        this.setState({
            studentName: "",
            collegeName: "",
            branch: "",
            gender: "",
            dateOfBirth: "",
            emailId: "",
            phone: "",
            address: ""
        })
    }

    render() {
        const { studentName, isSubmitted ,Error} = this.state

        if (isSubmitted) {
            return (
                <Success studentName={studentName} />
            )
        } else {
            return (
                <form noValidate onSubmit={this.onAddStudent}>
                    <label htmlFor="stdName" className="label-text">Student Name</label>
                    <input id="stdName" className="input" type="text" placeholder="Enter your name" onChange={this.onstudentName} />
                    <span className="error-msg">{Error.nameError}</span>

                    <label htmlFor="clgName" className="label-text">College Name</label>
                    <input id="clgName" className="input" type="text" placeholder="Enter college name"  onChange={this.onCollegeName} />
                    <span className="error-msg">{Error.clgNameErr}</span>

                      
                    <label htmlFor="clgName" className="label-text">Branch</label>
                    <select onChange={this.onbrachInput} className="branch">
                        <option>Select</option>
                        <option value="CSE">CSE</option>
                        <option value="Mech">Mech</option>
                        <option value="Civil">Civil</option>
                        <option value="Electronics">Electronics</option>
                    </select>
                    <span className="error-msg">{Error.branchErr}</span>

                    <h1 className="gender-heading">Gender</h1>
                    <input id="radio-male" type="radio" name="gender" value="Male" onChange={this.onMaleGenderInput} />
                    <label htmlFor="radio-male" className="radio-label-text">Male</label>
                    <input id="radio-female" type="radio" name="gender" value="Female" onChange={this.onFemaleGenderInput} />
                    <label htmlFor="radio-female" className="radio-label-text">Female</label>
                    <br/>
                    <span className="error-msg">{Error.genderErr}</span>

                    <label className="date-text">Date of birth</label>
                    <input className="input" type="date"  onChange={this.onDateOfBirth} />
                    <span className="error-msg">{Error.DOBErr}</span>

                    <label htmlFor="email" className="label-text">Email address</label>
                    <input id="email" className="input" type="email" placeholder="Enter email"  onChange={this.onEmailInput} />
                    <span className="error-msg">{Error.emailErr}</span>

                    <label htmlFor="phone" className="label-text">Phone Number</label>
                    <input id="phone" className="input" type="number" placeholder="Enter PhoneNumber"  onChange={this.onPhoneNumberInput} />
                    <span className="error-msg">{Error.phoneNumErr}</span>

                    <label htmlFor="address" className="label-text">Address</label>
                    <textarea id="adress" cols="37" rows="5" placeholder="Enter address" onChange={this.onAddressInput}></textarea>
                    <br/>
                    <span className="error-msg">{Error.addressErr}</span>
                    <button className="button" type="submit">Submit</button>
                </form>
            )
        }
    }
}


export default Form


