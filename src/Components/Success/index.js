import "./index.css"


const Success = props => {
    let { studentName } = props
    return (
        <h1>Thank you {studentName}</h1>
    )
}

export default Success