import Form from "./Components/Form"
import Header from "./Components/Header"

import './App.css';

function App() {
  return (
    <div className="main-container">
      <div>
        <Header/>
       <Form/>
       </div>
    </div>
  );
}

export default App;
